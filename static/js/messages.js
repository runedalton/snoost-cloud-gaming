var callbacks = {};
var callbacks_ids = 1;

var sendMessage = function(method, params) {
  return new Promise(function(resolve, reject) {
    var id = callbacks_ids++;
    callbacks[id] = {'resolve': resolve, 'reject': reject};
    
    common.naclModule.postMessage({ 'callbackId': id, 'method': method, 'params': params });
  });
};

function handleMessage(msg) {
  if(msg.data.callbackId && callbacks[msg.data.callbackId]) {  // if it's a callback, treat it as such
    callbacks[msg.data.callbackId][msg.data.type](msg.data.ret);
    delete callbacks[msg.data.callbackId];
  } else {
    //console.log(msg.data);
    if(msg.data === 'streamTerminated') {
      showSection('main');
      //displayError('Stream was terminated. Please try again.', 'warning');

      cloudRig.refreshServerInfo().then(function(ret) {  // refresh the serverinfo to acknowledge the currently running app
        cloudRig.getAppList().then(function(appList) {
          setCurrentGameClass();
        });
        scanGames();

        isInGame = false;

        // restore main window from 'fullscreen' to 'normal' mode (if required)
        (windowState == 'normal') && chrome.app.window.current().restore();
      });

    } else if(msg.data === 'Connection Established') {
      $('#loadingSpinner').css('display', 'none');
    } else if(msg.data.indexOf('ProgressMsg: ') === 0) {
      showLoadingMessage(msg.data.replace('ProgressMsg: ', '').replace(' establishment', '').replace(' initialization', '').replace('Starting RTSP handshake', 'Authorizing with Cloud Rig'));
    } else if(msg.data.indexOf('TransientMsg: ') === 0) {
      console.warn(msg.data.replace('TransientMsg: ', ''));
    } else if(msg.data.indexOf('DialogMsg: ') === 0) {
      // FIXME: Really use a dialog
      console.warn(msg.data.replace('DialogMsg: ', ''));
    } else if(msg.data === 'displayVideo') {
      $(".section").hide();
      playGameMode();
      $("#listener").addClass("fullscreen");
    }
  }
}
