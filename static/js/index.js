var activePolls = {}; // hosts currently being polled.  An associated array of polling IDs, keyed by server UID
var pairingCert;
var myUniqueid;
var isInGame = false;
var windowState = 'normal'; // 'normal' / 'fullscreen'
var firstLoad = true; // used for authenticator to know if it's the first load or not (no error messages should be shown if it's an automatic login attempt upon loading
var cloudRig = false;
var cloudServer = false;
var updateInverval = 5000;
var updatePolling = false;
var progressCounting = false;
var frameWidth = 0;
var frameHeight = 0;
var elmActionTimeout = 0;
var activeProcesses = {};
var gamefileLoaded = [];
var versionChecked = false;
var isPairing = false;
var gamesDoneLoading = false;
var previousExternalIP = false;


function attachListeners() {
  showLoadingMessage('Loading Snoost...');

  $("ul#controls").addClass("hide");

  $('#quitCurrentApp').on('click', stopGameWithConfirmation);
  $(window).resize(fullscreenNaclModule);
  chrome.app.window.current().onMaximized.addListener(fullscreenChromeWindow); // Is this even working? What is it?
}

function progressCounter(start, to, expectedTimeLeft) {
  var duration = expectedTimeLeft*1.3;

  if(!progressCounting) {
    progressCounting = new CountUp("currentLoad", start, to, 0, duration, { useEasing : false });
    progressCounting.start();
  } else {
    progressCounting.duration = duration*1000;
    progressCounting.update(to);
  }

  $("#endLoad").text(to);
}

function fullscreenChromeWindow() {
  // when the user clicks the maximize button on the window,
  // FIRST restore it to the previous size, then fullscreen it to the whole screen
  // this prevents the previous window size from being 'maximized',
  // and allows us to functionally retain two window sizes
  // so that when the user hits `esc`, they go back to the "restored" size, 
  // instead of "maximized", which would immediately go to fullscreen
  chrome.app.window.current().restore();
  chrome.app.window.current().fullscreen();
}

function onFullscreened() {
  if(!isInGame && windowState == 'normal') {
    windowState = 'fullscreen';
    $("#galaxybg").stop().fadeTo(2000, 0.1, function() { $(this).hide(); });
  }
}

function onBoundsChanged() {
  if(!isInGame && windowState == 'fullscreen') {
    windowState = 'normal';
  }
  $("#main").removeClass("fullscreen");
  $("#galaxybg").stop().show().fadeTo(1500, 1);
}

function showLoadingMessage(message) {
  $(".section").hide();
  $("#loading.section").show();

  $('#naclSpinnerMessage').text(message);
  $('#naclSpinner').css('display', 'inline-block');

  console.log(message);
}
function showSection(section) {
  $(".section").hide();
  $(".section#"+section).show();

  $("#listener").removeClass("fullscreen").stop().fadeTo(1, 1);
  chrome.app.window.current().restore();
}


function beginBackgroundPollingOfSnoost(newUpdateInverval) {
  if(updatePolling && newUpdateInverval == updateInverval) { return; }

  if(updatePolling) { stopBackgroundPollingOfSnoost(); }

  if(newUpdateInverval !== undefined && newUpdateInverval > 0) { updateInverval = newUpdateInverval; }

  updatePolling = window.setInterval(function() {
    sendRequest("rig_update=true");
  }, updateInverval);
}
function stopBackgroundPollingOfSnoost() {
  window.clearInterval(updatePolling);
  updatePolling = false;
}

function beginBackgroundPollingOfHost() {
  cloudRig.warmBoxArtCache();
  if(cloudRig.online) {
    activePolls[cloudRig.serverUid] = window.setInterval(function() {
      cloudRig.pollServer(function () {
        if(!cloudRig.online) {
          console.error("Cloud rig is not online");
          // TODO: NOTIFY OF DISCONNECT (CLOUD RIG IS NOT ONLINE FOR SOME REASON)
        }
      });
    }, 5000);
  } else {
    console.error("the cloud rig is offline - sure it's on?");
  }
}

function stopBackgroundPollingOfHost() {
  window.clearInterval(activePolls[cloudRig.serverUid]);
  delete activePolls[cloudRig.serverUid];
}

function moduleDidLoad() {
  if(!myUniqueid) {
    console.log("Failed to get uniqueId.  We should have already generated one.  Regenerating...");
    myUniqueid = uniqueid();
    storeData('uniqueid', myUniqueid, null);
  }

  loadCertificate(undefined, function() { Authenticate(); });
}
function loadCertificate(response, callbackFunction) {
  callbackTimeout = 500;
  if(response !== undefined) {
    if(previousExternalIP !== response.PublicIP) {
      pairingCert = false;
      cloudRig = false;
      callbackTimeout = 15000;
    }
  }
  if(!pairingCert) { // we couldn't load a cert. Make one.
    console.log("Failed to load local cert. Generating new one");
    sendMessage('makeCert', []).then(function (cert) {
      storeData('cert', cert, null);
      pairingCert = cert;
      console.log("Generated new cert.");
    }, function(failedCert) {
      console.log('ERROR: failed to generate new cert!');
      console.log('Returned error was: '+failedCert);
    }).then(function(ret) {
      if(callbackTimeout > 5000) {
        setTimeout(function() {
          showLoadingMessage('Enabling streaming');
        }, callbackTimeout/2);
      }
      setTimeout(function() {
        sendMessage('httpInit', [pairingCert.cert, pairingCert.privateKey, myUniqueid]).then(function(ret) {
          if(callbackFunction !== undefined && typeof callbackFunction == "function") {
            callbackFunction();
          }
        }, function(failedInit) {
          console.log('ERROR: failed httpInit!');
          console.log('Returned error was: '+failedInit);
        });
      }, callbackTimeout);
    });
  } else {
    setTimeout(function() {
      sendMessage('httpInit', [pairingCert.cert, pairingCert.privateKey, myUniqueid]).then(function(ret) {
        if(callbackFunction !== undefined && typeof callbackFunction == "function") {
          callbackFunction();
        }
      }, function(failedInit) {
        console.log('ERROR: failed httpInit!');
        console.log('Returned error was: '+failedInit);
      });
    }, callbackTimeout);
  }
}

function pairTo(nvhttpHost, onSuccess, onFailure) {
  if(!pairingCert) {
    displayError('ERROR: cert has not been generated yet. Is NaCl initialized?', 'danger');
    console.log("User wants to pair, and we still have no cert. Problem = very yes.");
    onFailure();
    return;
  }
  isPairing = true;

  nvhttpHost.pollServer(function(ret) {
    if(!nvhttpHost.online) {
      displayError('Failed to connect to the Cloud Rig! Are you sure the host is on?', 'danger');
      //console.log(nvhttpHost.toString());
      //throw new Error("Something went badly wrong!");
      onFailure();
      return;
    }
    if(!nvhttpHost || (nvhttpHost.connectionFailed !== undefined && nvhttpHost.connectionFailed !== false)) {
      cloudRig = false; nvhttpHost = false;
      console.log("Logoff");
      sendRequest("logoff=true", function() {
        setTimeout(function() {
          console.log("Try pairing again");
          beginBackgroundPollingOfSnoost(3000);
        }, 5000);
      });
      showLoadingMessage('Loading network configuration');
      return false;
    }

    if(nvhttpHost.paired) { onSuccess(); return; }

    var randomNumber = String("0000"+(Math.random()*10000|0)).slice(-4);
    showLoadingMessage('Configuring network');

    setTimeout(function() { showLoadingMessage('Reaching cloud gaming server'); }, 1000);
    setTimeout(function() {
      showLoadingMessage('Establishing connection');

      setTimeout(function() {
        showLoadingMessage('Opening cloud gaming server');

        sendRequest('pairTo='+randomNumber);
      }, 2000);
    }, 2000);

    nvhttpHost.pair(randomNumber).then(function(paired) {
      if(!paired) {
        if(nvhttpHost.currentGame !== 0) {
          displayError('Cloud Rig is busy. Stop streaming to pair.', 'danger');
        } else {
          displayError('Error: failed to pair with Cloud Rig', 'danger');
        }
        cloudRig = false; nvhttpHost = false;
        isPairing = true;
        console.log("Logoff");
        sendRequest("logoff=true", function() {
          showLoadingMessage('Trying to connect again');
          setTimeout(function() {
            isPairing = false;
            beginBackgroundPollingOfSnoost(3000);
            showLoadingMessage('Loading network configuration');
          }, 5000);
        });
        onFailure();
        return;
      }

      showSection('main');
      displaySuccess('Successfully connected');
      $("#controls #rePair").removeClass("disabled");
      $("#controls #rePair span").html("Reconnect");
      onSuccess();
      isPairing = false;
    }, function(failedPairing) {
      displayError('Failed pairing to Cloud Rig', 'danger');
      //console.log('Pairing failed, and returned '+failedPairing);
      isPairing = true;
      console.log("Logoff");
      sendRequest("logoff=true", function() {
        showLoadingMessage('Trying to connect again');
        setTimeout(function() {
          isPairing = false;
          beginBackgroundPollingOfSnoost(3000);
          showLoadingMessage('Loading network configuration');
        }, 5000);
      });
      //console.log('failed API object 2: ');
      //console.log(nvhttpHost.toString());
      onFailure();
    });
  });
}

function setCurrentGameClass() {
  if(cloudRig.currentGame === undefined || cloudRig.currentGame === 0) {
    $('#gameGrid img').removeClass("current-game");
    $("#quitCurrentApp").addClass("hide");
    return;
  }

  $('#gameGrid img').not('#game-'+cloudRig.currentGame).removeClass("current-game");
  $('#game-'+cloudRig.currentGame).addClass("current-game");
  $("#quitCurrentApp").removeClass("hide");
}

function showApps() {
  if(!cloudRig || !cloudRig.paired) {
    displayError('Moved into showApps, but the Cloud Rig did not initialize properly! Failing.', 'danger');
    return;
  }
  showBackgroundLoading("Loading system...");
  $("#gameGrid").addClass("visible");
  $("#rigLoadData").removeClass("visible");

  var activeGames = [];

  cloudRig.getAppList().then(function(appList) {
    var i = 0;
    var loadDelay = 50;
    appList.forEach(function(app) {
      i++;
      activeGames[app.id] = true;
      if($('#game-'+app.id).length === 0 && app.title == 'Windows') {
        $("#gameGrid").append($("<div>", { html: $("<img>", { src: "static/res/no_app_image.png", id: 'game-'+app.id, class: 'img-responsive', name: app.title }), class: 'col-sm-3 col-sm-offset-6 box-art holoborder', 'gameId': app.id }).append($("<span>", { html: app.title, class: "game-title" })).append($("<div>", { class: 'holoborderextension' })));

        $('#game-'+app.id).on('click', function() {
          if($("#gameGrid").hasClass("loading")) { return; }
          startGame(app.id);
        });

        cloudRig.getBoxArt(app.id).then(function(resolvedPromise) {
          var imageBlob;
          if(typeof resolvedPromise !== "string") {
            imageBlob =  new Blob([resolvedPromise], { type: "image/png" });
            imageBlob = URL.createObjectURL(imageBlob);
          } else { imageBlob = resolvedPromise; }

          $('#game-'+app.id).ready(function() {
            $('#game-'+app.id).attr("src", imageBlob);
            setTimeout(function() {
              $('#game-'+app.id).parent().addClass("loaded");
            }, loadDelay);
            loadDelay += 150;
          });
        }, function(failedPromise) {});
      }
      if(i == appList.length) { gameListLoaded(activeGames, loadDelay); }
    });
  }, function(failedAppList) {
    $('#naclSpinner').hide();
    console.error('Failed to get applist from host: '+cloudRig.hostname);
    displayError('Connection lost. Please restart the Chrome Client to re-connect to your cloud rig.', 'warning');
    //scanGames();
    //console.error('failed host object: ');
    //console.log(cloudRig.toString());
  });
}

function gameListLoaded(activeGames, loadDelay) {
  stopBackgroundLoading();
  setCurrentGameClass();
  showSection('main');
  $("#gameGrid div.box-art").each(function() {
    if(activeGames[$(this).attr("gameId")] === undefined) { $(this).remove(); }
    else if(!$(this).hasClass("loaded")) {
      var $this = $(this);
      setTimeout(function() { $this.addClass("loaded"); }, loadDelay);
      loadDelay += 150;
    }
  });
}


function startGame(appID) {
  if(!cloudRig || !cloudRig.paired) {
    console.error('Attempted to start a game, but the cloud rig did not initialize properly. Failing!');
    isPairing = false;
    rePair();
    return;
  }
  playGameMode();

  // refresh the server info, because the user might have quit the game.
  cloudRig.refreshServerInfo().then(function(ret) {
    cloudRig.getAppById(appID).then(function(appToStart) {

      if(cloudRig.currentGame !== 0 && cloudRig.currentGame != appID) {
        cloudRig.getAppById(cloudRig.currentGame).then(function(currentApp) {
          var quitAppDialog = document.querySelector('#quitAppDialog');

          var apptitle;
          if(currentApp !== null) { apptitle = currentApp.title; }
          else { apptitle = 'the program'; }

          $('#quitAppDialogText').text('A program is already running. Would you like to quit '+apptitle+'?');
          quitAppDialog.showModal();
          $('#cancelQuitApp').off('click');
          $('#cancelQuitApp').on('click', function () {
            quitAppDialog.close();
          });
          $('#continueQuitApp').off('click');
          $('#continueQuitApp').on('click', function () {
            stopGame(function() { startGame(appID); });
            quitAppDialog.close();
          });

          return;
        }, function(failedCurrentApp) {
          console.log('ERROR: failed to get the current running app from host!');
          //console.log('Returned error was: '+failedCurrentApp);
          //console.log('failed host object: ');
          //console.log(cloudRig.toString());
          return;
        });

        return;
      }

      showLoadingMessage('Starting '+appToStart.title+'...');
      setFullscreen(function() {
        var frameRate = "60";
        var rikey = generateRemoteInputKey();
        var rikeyid = generateRemoteInputKeyId();

        sendRequest('startGame='+$(".qualityOption.selected").find('input[type="radio"]').val()+'&rikey='+rikey+'&rikeyid='+rikeyid.toString()+'&appVersion='+cloudRig.appVersion+'&appTitle='+appToStart.title, function(request) {
          response = JSON.parse(request.responseText);

          if(response.ErrorMsg !== undefined) {
            showSection('main');
            setTimeout(function() {
              minimizeStream();
              displayError(response.ErrorMsg, 'danger');
            }, 1500); // TODO: This is just a guess... Perhaps figure out a callback solution to make sure minimizeStream is called exactly when windowState is fully "fullscreened" so sections etc. aren't hidden when displaying error message.
            return;
          }

          if(cloudRig.currentGame == appID) {
            return cloudRig.resumeApp(rikey, rikeyid).then(function(ret) {
              sendMessage('startRequest', response.Configuration);
            }, function(failedResumeApp) {
              console.error('ERROR: failed to resume the app!');
              console.log('Returned error was: '+failedResumeApp);
              return;
            });
          }

          cloudRig.launchApp(appID, frameWidth+"x"+frameHeight+"x"+frameRate,
            1, // Allow GFE to optimize game settings
            rikey, rikeyid,
            0, // Don't play audio on cloud rig
            0x030002 // Surround channel mask << 16 | Surround channel count
          ).then(function(ret) {
            sendMessage('startRequest', response.Configuration);
          }, function(failedLaunchApp) {
            console.error('ERROR: failed to launch app with appID: '+appID);
            console.log('Returned error was: '+failedLaunchApp);
            return;
          });
        }, function() {
          setTimeout(function() {
            minimizeStream();
          }, 1500); // TODO: Just a guess. Somehow identify when it's ready to be minimized.
        });
      });
    });
  });
}

function playGameMode() {
  isInGame = true;
  return;
  /*
  showLoadingMessage('Opening stream');
  $("#nacl_module").fadeTo(0, 0);
  isInGame = true;

  setTimeout(function() {
    //$(".section").hide(); $("#nacl_module").fadeTo(250, 1);
    setFullscreen();
    //fullscreenNaclModule();
  }, 1500);
  */
}

function fullscreenNaclModule() {
  var streamWidth = "1280"; 
  var streamHeight = "720";
  //var screenWidth = window.innerWidth;
  //var screenHeight = window.innerHeight;

  frameWidth = window.innerWidth;
  frameHeight = window.innerHeight;

  //var xRatio = screenWidth / streamWidth;
  //var yRatio = screenHeight / streamHeight;

  //var zoom = Math.min(xRatio, yRatio);

  var module = $("#nacl_module")[0];
  //module.width = zoom * streamWidth;
  //module.height = zoom * streamHeight;
  module.width = frameWidth;
  module.height = frameHeight;
  module.style.paddingTop = ((frameHeight - module.height) / 2) + "px";
}

function stopGameWithConfirmation() {
  if(cloudRig.currentGame === 0) {
    setCurrentGameClass();
    displayError('Nothing is running', 'danger');
  } else {
    console.log('Quitting app: '+cloudRig.currentGame);
    $("#quitCurrentApp .game-title").html('<i class="fa fa-spin fa-spinner"></i>');

    cloudRig.getAppById(cloudRig.currentGame).then(function(currentGame) {
      $("#quitCurrentApp .game-title").html('Quit Current App');
      var quitAppDialog = document.querySelector('#quitAppDialog');

      var apptitle;
      if(currentGame !== null) { apptitle = currentGame.title; }
      else { apptitle = 'the program'; }

      $('#quitAppDialogText').html('Are you sure you would like to quit '+apptitle+'? Unsaved progress will be lost.');
      quitAppDialog.showModal();
      $('#cancelQuitApp').off('click');
      $('#cancelQuitApp').on('click', function() {
        quitAppDialog.close();
      });
      $('#continueQuitApp').off('click');
      $('#continueQuitApp').on('click', function() {
        stopGame();
        quitAppDialog.close();
      });
    });
  }
}

function rePair() {
  if(isPairing) { return; }

  cloudRig = false;
  sendRequest();
  console.log('Re-connecting to cloud gaming server');
  $("#controls #rePair").addClass("disabled");
  $("#controls #rePair span").html("<i class=\"fa fa-spin fa-spinner\"></i>");
  $("#gameGrid").addClass("loading");
}

function scanGames() {
  if(isInGame) { return; }
  //if(!isPairing) { return; }
  if($("#main").hasClass("fullscreen")) { return; }
  if(!cloudRig.paired) {
    console.log('Not paired, trying to scan again');
    setTimeout(function() { scanGames(); }, 2500);
    return;
  }
  if(activeProcesses.scanningGames !== undefined) { return; }

  activeProcesses.scanningGames = true;
  console.log('Scanning for games');

  showBackgroundLoading('Scanning for games');
  $("#controls #scanGames").addClass("disabled");
  $("#controls #scanGames span").html("<i class=\"fa fa-spin fa-spinner\"></i>");
  $("#gameGrid").addClass("loading");
  gamesDoneLoading = false;
  gamefileLoaded = [];

  setTimeout(function() {
    showApps();
    $("#gameGrid").removeClass("loading");
    $("#controls #scanGames").removeClass("disabled");
    $("#controls #scanGames span").html("Re-scan system");
    activeProcesses.scanningGames = undefined;
  }, 2500); /* TODO: This is just a guess and not an accurate delay. */
}

function stopGame(callbackFunction) {
  isInGame = false;

  if(!cloudRig.paired) { displayError('Not paired!', 'danger'); return; }

  cloudRig.refreshServerInfo().then(function(ret) {
    cloudRig.getAppById(cloudRig.currentGame).then(function(runningApp) {
      if(!runningApp) { displayError('Nothing is running', 'warning'); return; }

      var appName = runningApp.title;
      showLoadingMessage('Stopping '+appName);

      cloudRig.quitApp().then(function(ret2) { 
        cloudRig.refreshServerInfo().then(function(ret3) {
          showSection('main');
          setCurrentGameClass();
          displaySuccess(appName+' successfully closed');
          if(typeof(callbackFunction) === "function") callbackFunction();
        }, function(failedRefreshInfo2) {
          showSection('main');
          console.log('ERROR: failed to refresh server info!');
          console.log('Returned error was: '+failedRefreshInfo2);
          console.log('failed server was: '+cloudRig.toString());
        });
      }, function(failedQuitApp) {
        showSection('main');
        setCurrentGameClass();
        console.log('ERROR: failed to quit app!');
        console.log('Returned error was: '+failedQuitApp);
      });
    }, function(failedGetApp) {
      showSection('main');
      console.log('ERROR: failed to get app ID!');
      console.log('Returned error was: ' + failedRefreshInfo);
    });
  }, function(failedRefreshInfo) {
    showSection('main');
    console.log('ERROR: failed to refresh server info!');
    console.log('Returned error was: ' + failedRefreshInfo);
  });
}

function storeData(key, data, callbackFunction) {
  var obj = {};
  obj[key] = data;
  if(chrome.storage)
      chrome.storage.sync.set(obj, callbackFunction);
}
function clearData(key) {
  if(chrome.storage) { chrome.storage.sync.remove(key); }
}


function Authenticate() {
  if(chrome.storage) {
    showLoadingMessage('Authenticating');
    console.log("Authenticating");

    chrome.storage.sync.get('LoginToken_Client', function(v) {
      var loginData = false;

      var xhr = new XMLHttpRequest();
      xhr.open("POST", "https://app.snoost.com/api/", true);
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

      if(v.LoginToken_Client !== undefined) {
        loginData = "LoginToken_Client="+v.LoginToken_Client+"&login=1";
      }
      if(!loginData) {
        if($("#loginForm #Email").val().length < 6) { showLogin(); displayError('Please enter your e-mail address', 'warning'); return false; }

        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if(!regex.test($("#loginForm #Email").val())) { showLogin(); displayError('Please enter a valid e-mail address', 'warning'); return false; }

        if($("#loginForm #Password").val().length < 8) { showLogin(); displayError('Password must consist of at least 8 characters', 'warning'); return false; }

        storeData('Email', $("#loginForm #Email").val());

        loginData = $("#loginForm").serialize();
      }

      if(frameHeight > 0 && frameWidth > 0) { loginData += "&displayWidth="+frameWidth+"&displayHeight="+frameHeight; }

      xhr.send(loginData);
      xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) { return handleLoginResponse(xhr); }
      };
    });
  }
}
function sendRequest(query, callbackFunction, timeoutFunction) {
  elmActionTimeout = 0;
  if(chrome.storage) {
    chrome.storage.sync.get('LoginToken_Client', function(v) {
      var loginData = false;

      var xhr = new XMLHttpRequest();
      xhr.open("POST", "https://app.snoost.com/api/", true);
      xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
      xhr.timeout = 6000;
      xhr.ontimeout = function() {
        if(timeoutFunction !== undefined && typeof timeoutFunction == "function") {
          displayError('Snoost timed out - Please try again later.', 'warning');
          timeoutFunction(xhr);
        }
        xhr.abort();
      };
      xhr.onerror = function () {
        if(timeoutFunction !== undefined && typeof timeoutFunction == "function") {
          displayError('Unable to connect - Please try again later by reloading the app.', 'warning');
          timeoutFunction(xhr);
        }
        xhr.abort();
      };

      if(v.LoginToken_Client !== undefined) { loginData = "LoginToken_Client="+v.LoginToken_Client+"&login=1"; }
      if(!loginData) { displayError("Session lost, please sign in again."); signOut(); }

      loginData += "&"+query;
      if(chrome.runtime.getManifest().version !== undefined) {
        loginData += "&clientVersion="+chrome.runtime.getManifest().version;
      }

      if(frameHeight > 0 && frameWidth > 0) { loginData += "&displayWidth="+frameWidth+"&displayHeight="+frameHeight; }

      xhr.send(loginData);
      xhr.onreadystatechange = function() {
        if(xhr.readyState == 4) {
          if(callbackFunction !== undefined && typeof callbackFunction == "function") { callbackFunction(xhr); }
          return handleLoginResponse(xhr);
        }
      };
    });
  }
}

function handleLoginResponse(request) {
  try {
    response = JSON.parse(request.responseText);
    cloudServer = response;
  } catch(err) {
    displayError('Unable to connect. Please try again later by reloading the app.', 'danger');
    console.error("Bad response");
    console.error(request.responseText);
    return false;
  }

  var errorMsg = false; var errorLevel = 'danger';
  if(request.status == 200) {
    if(firstLoad) {
      firstLoad = false;
      chrome.storage.sync.get('LoginToken_Client', function(v) {
        if(v.LoginToken_Client !== undefined) {
          $(".loginExternalLink").each(function() {
            var newHref = $(this).attr('href');
            if(newHref.indexOf("?") != -1) { newHref = newHref+'&'; }
            else { newHref = newHref+'?'; }
            newHref = newHref+'token='+v.LoginToken_Client;
            $(this).attr('href', newHref);
          });
          $(".loginExternalLink").removeClass("loading");
        } else { $(".loginExternalLink").addClass("loading"); }
      });
    }

    if(response.latestVersion != chrome.runtime.getManifest().version && !versionChecked) {
      console.log('Current version ('+chrome.runtime.getManifest().version+'). New version available ('+response.latestVersion+')');
      response.notificationText = 'A new version is available with security and performance improvements.<br /><small>To update, simply download the file and drag and drop it onto the "chrome://extensions" tab in your Chrome browser.</small></a>';
      response.notificationLinkText = '<i class="fa fa-download"></i><br />Get now';
      response.notificationLinkURL = response.latestVersionURL;
      versionChecked = true;
    }

    if($("#naclSpinnerMessage").text() == 'Authenticating') {
      showSection('main');
    }
  } else if(request.status == 400) { errorMsg = "Bad request"; console.log(request); }
  else if(request.status == 401) { errorMsg = "Unable to login"; }
  else if(request.status == 403) { errorMsg = "Missing login credentials"; errorLevel = 'warning'; }
  else if(request.status == 404) { errorMsg = "Unable to communicate with Snoost"; }

  if(request.status != 200) {
    if(response.Message) { displayError(response.Message, errorLevel); }
    else { displayError(errorMsg, errorLevel); }

    showLogin();
  } else {
    $("#notificationMessage").stop().fadeTo(250, 0);
    $("#Password").val("");
    handleSettingsResponse(response);
  }
}
function CheckLatency(regions, currentindex, checks) {
  if(currentindex === undefined) { currentindex = 0; }
  if(checks === undefined) { checks = 1; }

  var region = regions[currentindex];

  if(region !== undefined && region) {
    showBackgroundLoading('Checking latency...');

    var starttime = new Date().getTime();
    var xhr = new XMLHttpRequest();
    xhr.open("GET", region.endpoint, true); xhr.send();
    xhr.onreadystatechange = function() {
      if(xhr.readyState == 2) {
        var endtime = new Date().getTime();

        if(region.latency === undefined) { regions[currentindex].latency = []; }
        regions[currentindex].latency.push(endtime-starttime);

        var delay = 30; currentindex++;
        setTimeout(function() { CheckLatency(regions, currentindex, checks); }, delay);
      }
    };
  } else {
    if(checks > 4) {
      stopBackgroundLoading();
      console.log("Checked latencies");

      activeProcesses.testingLatencies = undefined;

      sendRequest('latency='+JSON.stringify(regions));
    } else {
      var delay = 250; checks++;
      setTimeout(function() { CheckLatency(regions, 0, checks); }, delay);
    }
  }
}
function showBackgroundLoading(msg) {
  if($("#backgroundLoading span").html() != msg) {
    $("#backgroundLoading span").html(msg);
  }
  $("#backgroundLoading").stop().show().fadeTo(250, 1);
}
function stopBackgroundLoading() {
  $("#backgroundLoading").stop().fadeTo(250, 0, function() { $(this).hide(); });
}

function handleSettingsResponse(response) {
  if(chrome.storage) {
    //if(response.State !== undefined) { storeData('State', response.State); }
    if(response.LoginToken_Client !== undefined) {
      chrome.storage.sync.get('LoginToken_Client', function(v) {
        if(v.LoginToken_Client !== null && v.LoginToken_Client != response.LoginToken_Client) {
          storeData('LoginToken_Client', response.LoginToken_Client);
        }
      });
    }

    if(response.CheckLatency !== undefined) {
      if(activeProcesses.testingLatencies === undefined) {
        activeProcesses.testingLatencies = true;
        var regions = JSON.parse(response.CheckLatency);
        CheckLatency(regions);
      }
    }

    if(response.trialExpired !== undefined && response.wasLogin === undefined) {
      showUpgradeModal();
    } else {
      if($("#qualitySetting").hasClass("loading") && response.setQuality === undefined && response.resolution !== undefined) {
        if(response.resolution > 3 && $(".qualityOption .selected").length < 1) {
          $(".qualityOption#quality_4").addClass("selected");
        } else {
          $(".qualityOption#quality_"+response.resolution).addClass("selected");
        }
        $("#qualitySetting").removeClass("loading");
        closeModal();
      }
    }

    if(response.notificationText !== undefined) {
      if(!$("#rigNotification").hasClass("visible")) {
        $("#rigNotification").html(response.notificationText);

        if(response.notificationLinkText !== undefined) {
          chrome.storage.sync.get('LoginToken_Client', function(v) {
            if(response.notificationLinkURL !== undefined) {
              $("#rigNotification").append('<br /><a href="'+response.notificationLinkURL+'" target="_blank" class="btn btn-default holoborder">'+response.notificationLinkText+'<div class="holoborderextension"></div></a>');
            } else if(v.LoginToken_Client !== undefined) {
              $("#rigNotification").append('<br /><a href="https://app.snoost.com/?token='+v.LoginToken_Client+'" target="_blank" class="holoborder">'+response.notificationLinkText+'<div class="holoborderextension"></div></a>');
            }
          });
        } else if(response.notificationClose !== undefined) {
          $("#rigNotification").append('<br /><button class="notificationClose holoborder">'+response.notificationClose+'<div class="holoborderextension"></div></button>');
          $("#rigNotification .notificationClose").one("click", function() {
            sendRequest('close_user_notice=1');
            $("#rigNotification").removeClass("visible");
          });
        }
        $("#rigNotification").addClass("visible");
      }
    }

    if(response.community !== undefined) {
      var loadCommunity = true;
      if(response.community.forums.constructor !== Array) { loadCommunity = false; }
      if(response.community.threads.constructor !== Array) { loadCommunity = false; }

      if(loadCommunity) {
        var timeoutSetting = 0;
        $.each(response.community.threads, function(index, thread) {
          timeoutSetting += 75;
          if(!$("#community ul.content li."+thread.threadClass)[0]) {
            $("#community .content").append('<li class="loading '+thread.threadClass+'" data-lastpost="'+thread.lastPost+'"><a href="'+thread.url+'" target="_blank"><i class="fa fa-envelope"></i> '+thread.subject+'</a></li>');
            setTimeout(function() { $('#community ul.content li.'+thread.threadClass).removeClass("loading"); }, timeoutSetting);
          } else {
            setTimeout(function() {
              $('#community ul.content li.'+thread.threadClass+' a').stop().fadeTo(300, 0.5);
              setTimeout(function() { $('#community ul.content li.'+thread.threadClass+' a').stop().fadeTo(200, 1); }, 500);
            }, timeoutSetting);
          }
        });
        setTimeout(function() {
          $("#community .content .loader").stop().animate({
            height: 0, opacity: 0
          }, 500, function() { $("#community .content .loader").remove(); });
          $("#community").addClass("loaded");
          $("#community ul.content li").sort(function(a, b) { return ($(a).data('lastpost')) < ($(b).data('lastpost')) ? 1 : -1; }).appendTo('#community ul.content');
        }, timeoutSetting);
      }
    }

    if(response.serverLocation !== undefined && response.bestLatency !== undefined) {
      if(!$("#rigLocation").is(":visible")) {
        $("#rigLocation .serverLocation").html(response.serverLocation);
        $("#rigLocation .bestLatency").html(response.bestLatency);
        $("#rigLocation").stop().show().fadeTo(500, 1);
      }
    } else if($("#rigLocation").is(":visible")) {
      $("#rigLocation").stop().fadeTo(250, 0, function() { $(this).hide(); });
    }

    if(response.Status !== undefined) {
      if(response.StatusMessages === undefined) {
        $("#rigStatus").html("Status: "+response.Status);
        if(response.StatusMessage !== undefined) { $("#rigStatus").append('<div class="extra small">'+response.StatusMessage+'</div>'); }
      }

      if(response.Status == 'Online' && response.Action !== 'turnoff') {
        $("#powerbutton").removeClass("on").addClass("off"); $("#powerbutton span").html("Turn off"); $("#rigStatus").hide();
        $("#rigLoadData").removeClass("visible");

        if(!$("#loading").is(":visible") && !$("#gameGrid").hasClass("visible") && $("ul#controls").hasClass("hide")) {
          showLoadingMessage('Loading cloud gaming desktop');
          setTimeout(function() {
            $("#rigLoadData .gameFiles").empty();
            $("#rigLoadData .statusMessages").empty();
            gamesDoneLoading = false;
            gamefileLoaded = [];
          }, 500);
        } else if($("#gameGrid").hasClass("visible")) {
          $("#rigLoadData .gameFiles").empty();
          $("#rigLoadData .statusMessages").empty();
          gamesDoneLoading = false;
          gamefileLoaded = [];
        }
      } else if(response.StatusMessages === undefined) {
        $("#rigStatus").show();
        $("#rigLoadData").removeClass("visible");
      } else if(response.Action === 'turnoff') {
        $("#rigLoadData").addClass("visible");
        $("#gameGrid").removeClass("visible");
        cloudRig.paired = false; isPairing = false; isInGame = false;
      } else {
        $("#rigLoadData").addClass("visible");
      }

      if(response.Status == 'In queue' || response.Status == 'Saving') {
        $("#gameGrid").removeClass("visible");
      }
      if(response.Status == 'Offline') {
        $("#powerbutton").removeClass("off").addClass("on"); $("#powerbutton span").html("Power on");
        $("#gameGrid").removeClass("visible");
        $("#rigLoadData").removeClass("visible");
        $("#rigLoadData .gameFiles").empty();
        $("#rigLoadData .statusMessages").empty();
        gamesDoneLoading = false;
        gamefileLoaded = [];
      }/*
      if(response.Action !== null) {
        cloudRig.paired = false;
      } */
    } else { $("#rigStatus").html("Unable to get cloud rig status"); }

    if(response.State !== null) {
      if(!cloudRig.paired) {
        if(response.Action === null && !isPairing && !isInGame) {
          showLoadingMessage('Preparing to connect');
        } else if(response.Action === 'turnon' || response.Action === 'turnoff') {
          if(response.StatusMessages !== undefined) {
            $("#rigStatus").hide();

            var timeout = 0;
            $.each(response.StatusMessages, function(key, value) {

              if(parseInt(key) === 0) {
                if(value == 'done') {
                  $("#rigLoadData .gameFiles").removeClass("visible");
                  return;
                } else {
                  $("#rigLoadData .gameFiles").addClass("visible");
                }

                var fileTimeout = 0;
                var i = 0;
                $("#rigLoadData .gameFiles").children('span').each(function() {
                  $(this).removeAttr('newTop');
                  $(this).removeAttr('newOpacity');
                });
                $.each(value, function(directory, size) {
                  var decimals = 0;
                  var measurement = 'KB';
                  if(directory.length < 2) { return; }
                  if(directory === 'total') {
                    size = size/1000; // MB
                    size = size/1000; // GB
                    decimals = 2;
                    measurement = 'GB';
                  }
                  i++;
                  var slug = directory.toLowerCase().replace(/[^\w ]+/g,'').replace(/ +/g,'-');
                  var topPosition = i*10;
                  if(i === 1) { topPosition = 0; }

                  var currentOpacity = 1-(i*0.05);
                  if(currentOpacity < 0) {
                    currentOpacity = 0;
                    topPosition = 300;
                  }
                  if($("#rigLoadData .gameFiles #gamefile"+slug).length < 1) {
                    $("#rigLoadData .gameFiles").append("<span id=\"gamefile"+slug+"\">"+directory+"<i><b id=\"gamefile"+slug+"_size\">0</b> "+measurement+"</i></span>");
                    gamefileLoaded["gamefile"+slug+"_size"] = new CountUp("gamefile"+slug+"_size", 0, size, decimals, 5, { useEasing : false });
                    gamefileLoaded["gamefile"+slug+"_size"].start();
                    setTimeout(function() {
                      $("#rigLoadData .gameFiles #gamefile"+slug).addClass("loaded");
                      $("#rigLoadData .gameFiles #gamefile"+slug).css({ top: "-40px", opacity: 0.2 });
                    }, fileTimeout);
                    fileTimeout += 150;
                  } else {
                    var previousSize = parseInt($("#gamefile"+slug+"_size").text().replace(/,/g , ""));
                    var sizeDifference = size-previousSize;
                    if(sizeDifference > 0) {
                      var seconds = sizeDifference;
                      if(gamesDoneLoading) { seconds = 2; }
                      else if(seconds < 10 && directory != 'total') { seconds = 3; }
                      else if(seconds < 100 && directory != 'total') { seconds = 5; }
                      else if(seconds < 1000 && directory != 'total') { seconds = 8; }
                      else if(seconds < 10000 && directory != 'total') { seconds = 10; }
                      else if(seconds < 50000 && directory != 'total') { seconds = 15; }
                      else if(seconds < 100000 && directory != 'total') { seconds = 20; }
                      else { seconds = 30; }

                      gamefileLoaded["gamefile"+slug+"_size"].duration = seconds*1000;
                      gamefileLoaded["gamefile"+slug+"_size"].update(size);
                    }
                  }
                  if(currentOpacity === 0) { gamefileLoaded["gamefile"+slug+"_size"].reset(); }
                  $("#rigLoadData .gameFiles #gamefile"+slug).attr('newTop', topPosition);
                  $("#rigLoadData .gameFiles #gamefile"+slug).attr('newOpacity', currentOpacity);
                });

                $("#rigLoadData .gameFiles span.loaded").each(function() {
                  var newTop = $(this).attr('newTop');
                  var newOpacity = $(this).attr('newOpacity');
                  if(typeof newTop !== typeof undefined && newTop !== false) {
                    $(this).css({ top: newTop+"px", opacity: newOpacity });
                  } else {
                    if(i > 10) {
                      $(this).css({ top: "-40px", opacity: 0 });
                    }
                  }
                });

                return;
              }

              if(value == 'Games loaded') { gamesDoneLoading = true; }
              if($("#rigLoadData .statusMessages .statusMessage"+key).length < 1) {
                value = "<i class=\"fa fa-spin fa-spinner\"></i> "+value;
                $("#rigLoadData .statusMessages").prepend("<span class=\"statusMessage"+key+"\">"+value+"</span>");
                setTimeout(function() {
                  $("#rigLoadData .statusMessages span.first").each(function() {
                    var icon = $(this).find("i");
                    var span = $(this);
                    icon.stop().fadeTo(250, 0);
                    setTimeout(function() {
                      icon.removeClass("fa-spin");
                      icon.removeClass("fa-spinner");
                      icon.addClass("fa-check");
                      span.removeClass("first");
                      icon.stop().fadeTo(250, 1);
                      setTimeout(function() { icon.removeAttr("style"); }, 300);
                    }, 300);
                  });

                  setTimeout(function() {
                    $("#rigLoadData .statusMessages .statusMessage"+key).addClass("loaded").addClass("first").css({ opacity: 1 });
                  }, 500);

                  var k = 0;
                  $("#rigLoadData .statusMessages span.loaded").each(function() {
                    var newOpacity = 1-(k*0.1);
                    $(this).css({ opacity: newOpacity });
                    k++;
                  });
                }, timeout);
                timeout += 750;
              } else {
                var currentvalue = $("#rigLoadData .statusMessages .statusMessage"+key).html().replace(/ style=""/g , "");
                loadingvalue = "<i class=\"fa fa-spin fa-spinner\"></i> "+value;
                if(currentvalue != loadingvalue && (loadingvalue.indexOf('Starting') > -1 || loadingvalue.indexOf('Saving batch of') > -1)) {
                  $("#rigLoadData .statusMessages .statusMessage"+key).html(loadingvalue);
                } else if(currentvalue != loadingvalue) {
                  value = "<i class=\"fa fa-check\"></i> "+value;
                  if(currentvalue != value) {
                    setTimeout(function() {
                      $("#rigLoadData .statusMessages .statusMessage"+key).css({ opacity: 0.01 });
                      setTimeout(function() {
                        $("#rigLoadData .statusMessages .statusMessage"+key).html(value);
                        var k = 0;
                        $("#rigLoadData .statusMessages span.loaded").each(function() {
                          var newOpacity = 1-(k*0.1);
                          $(this).css({ opacity: newOpacity });
                          k++;
                        });
                      }, 300);
                    }, timeout);
                    timeout += 500;
                  }
                }
              }
            });

            beginBackgroundPollingOfSnoost(5000);
          }
        }
      }
    }

    if(!cloudRig.paired) {

      if(readyToPair(response) && !isPairing && !isInGame) {
        showLoadingMessage('Connecting');

        isPairing = true;
        sendRequest("logoff=true");
        loadCertificate(response, function() {
          //sendMessage('httpInit', [pairingCert.cert, pairingCert.privateKey, myUniqueid]).then(function(ret) {
            if(!cloudRig) { cloudRig = new NvHTTP(response.DNS, myUniqueid, response.DNS); }

            showLoadingMessage('Loading stream settings');

            pairTo(cloudRig, function() {
              $("#controls #rePair").removeClass("disabled");
              $("#controls #rePair span").html("Reconnect");
              beginBackgroundPollingOfHost();
              beginBackgroundPollingOfSnoost(30000);
              saveCloudRig();
              initializeCloudRig();
  
              displaySuccess('Successfully connected');
              playSound('ready');
              isPairing = false;
              sendRequest('connected=true');
  
              showSection('main');
            }, function() { displayError('Pairing to cloud rig failed!', 'danger'); });
          //});
        });
      } else {
        if(response.Action !== null) {
          beginBackgroundPollingOfSnoost(5000);
          if(cloudRig.paired) {
            console.log("Cloud gaming server is already paired. Resuming activity.");
            showSection('main');
          }
        } 
      }
    }

    var controlsHidden = false;
    if(response.Action === undefined) { controlsHidden = true; }
    //else if(response.Action !== undefined && response.Action !== null) { controlsHidden = true; }
    else if(response.Steam === undefined || response.Steam !== 'ready') { controlsHidden = true; }
    else if(response.Status !== undefined && response.Status !== 'Online') { controlsHidden = true; }
    else if(response.Action !== undefined && response.Action === 'turnoff') { controlsHidden = true; }

    if(controlsHidden) {
      $("ul#controls").addClass("hide");
    } else { $("ul#controls").removeClass("hide"); }

  } else { displayError('Unable to load Chrome storage', 'danger'); }
}
function saveCloudRig() {
  cloudRig._prepareForStorage();
  storeData('cloudRig', cloudRig, null);
}
function initializeCloudRig() {
  if(!cloudRig) { displayError('Unable to initialize Cloud Rig', 'danger'); return false; }
  if(!cloudRig.online) { displayError('Cloud Rig is not online', 'danger'); return false; }

  console.log('Initializing cloud rig');

  stopBackgroundPollingOfHost();
  if(!cloudRig.paired) {
    console.error('Still not paired; go to the pairing flow'); // this generally should never happen
    pairTo(cloudRig, function() {
      //scanGames();
      saveCloudRig();
    }, function() {});
  } else {
    console.log("Scanning for games on Cloud Rig initialisation");
    setTimeout(function() {
      scanGames();
    }, 1000);
  }
}

function readyToPair(response) {
  if(response.DNS === undefined || response.DNS === null) { return false; }
  if(response.Action !== null && response.Action !== 'turnon') { return false; }
  if(response.Status !== "Online") { return false; }
  if(response.State !== "running") { return false; }
  if(response.Steam !== "ready") { return false; }
  if(isPairing) { return false; }

  return true;
}

function displayAlert(alertMessage, alertType) {
  $("#notificationMessage").stop().fadeTo(150, 0, function() {
    $(this).stop().addClass("text-"+alertType).html(alertMessage).show().fadeTo(250, 1, function() {
      $("#notificationMessage").stop().fadeTo(10000, 0, function() { $(this).hide().empty(); });
    });
  });
}
function displayError(errorMessage, errorLevel) {
  if(firstLoad) { firstLoad = false; return; }

  displayAlert(errorMessage, errorLevel);

  if(errorLevel == 'danger') { console.error(errorMessage); }
  else { console.warn(errorMessage); }
}
function displaySuccess(successMessage) {
  displayAlert(successMessage, 'success');

  console.log(successMessage);
}

function signOut() {
  showLogin();
  /* TODO: Power off if it is powered on */
}

function resetData() {
  clearData('Action');
  clearData('State');
  clearData('Status');
  clearData('LoginToken_Client');
  clearData('DNS');
  //clearData('uniqueid');
}

function showLogin() {
  $(".section").hide();
  $(".section#login").show();

  resetData();

  $("#loginForm").off("submit").one("submit", function(e) {
    e.preventDefault();

    Authenticate();

    return false;
  });
}

function setQualitySetting(elm) {
  if($("#qualitySetting").hasClass("loading")) { return; }
  $("#qualitySetting").addClass("loading");
  $(elm).parent().parent().find("div.qualityOption").attr("disabled", "disabled");

  showBackgroundLoading("Setting quality");

  var minTimeout = 750;
  var startTime = Date.now();

  sendRequest("setResolution="+$(elm).find('input[type="radio"]').val(), function(request) {
    response = JSON.parse(request.responseText);

    var delay = minTimeout-(Date.now() - startTime);
    if(delay < 50) { delay = 50; }
    setTimeout(function() {
      if(response.setQuality !== undefined && response.setQuality >= $(elm).find('input[type="radio"]').val()) {
        $(elm).parent().parent().find("div.qualityOption").removeClass("selected");
        $(elm).addClass("selected");
        $(elm).find('input[type="radio"]').prop("checked", true);
      } else {
        if(response.setQuality > 2 && response.setQuality < 6) {
          var newelm = "#quality_"+response.setQuality;
          $(newelm).parent().parent().find("div.qualityOption").removeClass("selected");
          $(newelm).addClass("selected");
          $(newelm).find('input[type="radio"]').prop("checked", true);
        }

        showUpgradeModal();
      }
      $("#qualitySetting").removeClass("loading");
      stopBackgroundLoading();
      $(elm).parent().parent().find("div.qualityOption").removeAttr("disabled");
    }, delay);
  }, function() {
    $("#qualitySetting").removeClass("loading");
    stopBackgroundLoading();
    $(elm).parent().parent().find("div.qualityOption").removeAttr("disabled");
  });
}

function closeModal() {
  $(".rigModal").stop().fadeTo(250, 0, function() {
    var $this = $(this);
    setTimeout(function() {
      $this.removeClass("loaded");
      $this.hide();
    }, 250);
  });
}
function showUpgradeModal() {
  if($("#selectPlan").is(':visible')) { return; }

  $("#selectPlan .rigModalLoader").html('Loading... <i class="fa fa-spin fa-spinner"></i>');

  $("#selectPlan").show().fadeTo(250, 1);
  $(".rigModal").removeClass("loaded");

  var minTimeout = 750;
  var startTime = Date.now();

  sendRequest('modal=plan', function(request) {
    var delay = minTimeout-(Date.now() - startTime);
    if(delay < 50) { delay = 50; }

    setTimeout(function() {
      var modalResponse = JSON.parse(request.responseText);

      $("#selectPlan").addClass("loaded");

      if(modalResponse.plans.headerText !== undefined) { $("#selectPlan .headerText").html(modalResponse.plans.headerText); }
      else { $("#selectPlan .headerText").empty(); }
      if(modalResponse.plans.headerText !== undefined) { $("#selectPlan .extraText").html(modalResponse.plans.extraText); }
      else { $("#selectPlan .extraText").empty(); }

      $("#selectPlan .rigModalContent .product-chooser").html(modalResponse.plans.html);

      setTimeout(function() {
        $("#selectPlan").find("div.product-chooser-item").on("click", function() {
          if($(this).hasClass("selected")) { return; }

          $("#selectPlan").find("div.product-chooser-item").removeClass("selected");
          if($(this).find(".dialog").length) { return; }
          var planId = $(this).find("input").val();

          if(modalResponse.plans.external === undefined) {
            $(this).addClass("selected");
            //show confirmation dialog and (if "ok" = sendRequest(upgradeplan))
            $("#selectPlan .dialog").stop().fadeTo(150, 0, function() { var $this = $(this); setTimeout(function() { $this.remove(); }, 200); });
            $(this).append('<div class="dialog">Please confirm your upgrade<br /><button class="btn btn-sm confirm">Confirm</button><button class="btn btn-sm cancel">Cancel</button></div>');
            var dialog = $(this).find(".dialog");
            $(this).find(".cancel").on("click", function() {
              dialog.stop().fadeTo(150, 0, function() { var $this = $(this); setTimeout(function() { $this.remove(); }, 200); });
            });
            $(this).find(".confirm").on("click", function() {
              dialog.html('Upgrading... <i class="fa fa-spin fa-spinner"></i>');
              sendRequest("upgradePlan="+planId, function(upgradeRequest) {
                var upgradeResponse = JSON.parse(upgradeRequest.responseText);

                setTimeout(function() {
                  if(upgradeResponse.upgradeSuccessfull !== undefined) {
                    dialog.html('<span class="text-success">Successfully upgraded <i class="fa fa-check"></i></span>');
                    $(".qualityOption").removeClass("selected");
                    $(".qualityOption#quality_"+upgradeResponse.upgradeSuccessfull).addClass("selected");
                    $("#qualitySetting").removeClass("loading");
                  } else {
                    if(upgradeResponse.errorMessage) { displayError(upgradeResponse.errorMessage, 'danger'); }
                    dialog.html('<span class="text-warning">Something went wrong, please try again later</span>');
                  }

                  setTimeout(function() {
                    dialog.stop().fadeTo(150, 0, function() { var $this = $(this); setTimeout(function() { $this.remove(); }, 200); });
                    closeModal();
                  }, 2000);
                }, 500);
              });
            });
            setTimeout(function() { $("#selectPlan .dialog").addClass("loaded"); }, 50);
          } else if(modalResponse.plans.external) {
            $("#selectPlan").removeClass("loaded");
            $("#selectPlan .rigModalLoader").html('Waiting... <i class="fa fa-spin fa-spinner"></i>');
            $("#qualitySetting").addClass("loading");
            window.open("https://app.snoost.com/?token="+modalResponse.plans.external+"&Billing=true&Plan="+planId);
            beginBackgroundPollingOfSnoost(5000);
          }
        });
      }, 500);
    }, delay);
  });
}

function onWindowLoad() {
  chrome.app.window.current().onFullscreened.addListener(onFullscreened);
  chrome.app.window.current().onBoundsChanged.addListener(onBoundsChanged);

  if(chrome.storage) {
    chrome.storage.sync.get('Email', function(v) { $('#Email').val(v.Email); });

    chrome.storage.sync.get('uniqueid', function(savedUniqueid) {
      if(savedUniqueid.uniqueid !== null) {
        myUniqueid = savedUniqueid.uniqueid;
      } else {
        myUniqueid = uniqueid();
        storeData('uniqueid', myUniqueid, null);
      }
    });

    // load the HTTP cert if we have one.
    chrome.storage.sync.get('cert', function(savedCert) {
      if(savedCert.cert !== null) {
        pairingCert = savedCert.cert;
      }
    });
    chrome.storage.sync.get('cloudRig', function(previousValue) {
      if(previousValue.cloudRig !== undefined && previousValue.cloudRig !== null) {
        host = previousValue.cloudRig;
        cloudRig = new NvHTTP(host.address, myUniqueid, host.userEnteredAddress);
        cloudRig.serverUid = host.serverUid;
        cloudRig.externalIP = host.externalIP;
        previousExternalIP = host.externalIP;
        cloudRig.hostname = host.hostname;
        cloudRig.wasPrevious = true;
        console.log('Loaded previously connected cloud rig.');
      } else { cloudRig = false; }
    });

    $("#qualitySetting").find("div.qualityOption").on("click", function() {
      setQualitySetting(this);
    });
  }

  $("#rigNotification").on("click", function() {
    if($(this).find(".notificationClose").length) { return; }
    $(this).removeClass("visible");
  });
  $("#controls #scanGames").on("click", scanGames);
  $("#controls #rePair").on("click", rePair);
  $("#community .header .fa-refresh").on("click", sendRequest);
  $("#signOut").on("click", signOut);
  $("#powerbutton").on("click", powerHandler);
  $(".rigModal .close").on("click", closeModal);

  $(document).on("keydown", function(e) {
    if(e.shiftKey && e.altKey && e.which == 87) { minimizeStream(); } // ALT+SHIFT+W
    if(e.shiftKey && e.altKey && e.which == 81) { quitStream(); } // ALT+SHIFT+Q
    if(e.shiftKey && e.altKey && e.which == 70) { setFullscreen(); } // ALT+SHIFT+F
    if(e.which == 27) { showOverlay(); } // ESC
    else { hideOverlay(); }
  });
  $(document).on("click", function() { hideOverlay(); });

  chrome.commands.onCommand.addListener(function(command) {
    if(command == 'minimize-stream') {
      minimizeStream();
    } else if(command == 'quit-stream') {
      quitStream();
    }
  });
}
function showOverlay() {
  $(".overlay").addClass("visible");
}
function hideOverlay() {
  $(".overlay").removeClass("visible");
}

function minimizeStream() {
  if(windowState != 'normal') { chrome.app.window.current().restore(); }
}
function quitStream() {
  if(isInGame) {
    //console.log("Scanning for games on quitting stream");
    $("#listener").stop().fadeTo(500, 0, function() {
      sendMessage('stopRequest', response.Configuration);
      //scanGames();
    });
  }
}

function setFullscreen(callbackFunction) {
  if(callbackFunction !== undefined) {
    chrome.app.window.current().onFullscreened.addListener(
      function runCallback() {
        chrome.app.window.current().onFullscreened.removeListener(runCallback);
        return callbackFunction();
      }
    );
  }

  $("#main").addClass("fullscreen");
  windowState = 'fullscreen';
  chrome.app.window.current().fullscreen();
}

function playSound(sound) {
  var audio = new Audio('static/sounds/'+sound+'.mp3');
  audio.play();
}

function powerHandler() {
  $("#powerbutton span").empty();

  if($("#powerbutton").hasClass("on")) {
    sendRequest('rig_action=TurnOn');
    gamefileLoaded = [];
    beginBackgroundPollingOfSnoost(5000);
  } else {
    sendRequest('rig_action=TurnOff');
    gamefileLoaded = [];
    beginBackgroundPollingOfSnoost(10000);
  }
  $("#powerbutton").removeClass("on").removeClass("off");
}

window.onload = onWindowLoad;