Based on the Moonlight Chrome project, this is a lightweight browser client (currently only works in Google Chrome) which allows users of Snoost to connect their local PCs to the remote external cloud gaming servers, and thus install and play most modern PC games on high graphics, and having them streamed directly to their own device.

**Table of Contents**

[TOC]


## About the Snoost Cloud Gaming project
Snoost was founded in early 2017, although the development had already been going on for more than a year, and the foundation simply marked the first time the project was ready to see the light. The game streaming client - which this article covers - is the third major client released to remotely access and control gaming PCs in the cloud, and was the result of a decision to make the client more available on all major operating systems, without having to build signed native programs for each different OS.

### From Steam In-Home Streaming to a newer client
NVIDIA and Moonlight was not integrated in the beginning, since the project relied on Steam�s In-Home Streaming and a rather complex OpenVPN setup to work, which was a moderately difficult process, since installing and preparing the gaming client to speak to the server usually required a lot of manual actions to be carried out by the user, and thus prompted a lot of challenges in the automation, which even took some UI manipulation - both on the server and client - to be configured properly on each startup of the gaming servers.

It ended up working really well though, and the Alpha version was well received with many users signing up wanting to try out the new cloud gaming service, despite the service having a somewhat unintuitive installing procedure. The game streaming worked smoothly and the user was able to play a wide variety of games, as long as the user was signed in on Steam on both the local PC as well as the cloud gaming server. Sometimes this required a "reconnect" on the VPN connection, since there were many problems with Steam's detection of users, which led to the implementation of various "cloud rig controls" which would enable to user to initiate tasks like VPN re-connect, restarting Steam, keep the game in the foreground, change locale etc.

The biggest limitation - except for the nonintuitive setup procedure - was the fact that Steam pretty much only had a limited capacity to control and adjust the bandwidth. This made it extremely difficult to properly optimise the game streaming to each individual user, and further expanded the requirement for more instructions for the user, in order to troubleshoot or optimise the gaming performance and the resolution. Also, it was rather complicated to automate bandwidth optimisation, since most of the options presented in Steam were inadequate for a lot of different scenarios. In the end, Snoost went through a lot of development and decided to work on a new browser based game streaming client, which was based on Moonlight Chrome.

### The Open-Source part of cloud gaming
The team behind Snoost is a passionate group of developers, with the common goal of making "streaming games from the cloud" a rather common thing in the world of video gaming. As a path to this goal, the client which communicates with the cloud gaming servers has been made open source to allow other developers and interested parts to see what's going on, contribute with ideas and commits, and in the end have an impact on the future of cloud gaming - whether it's for Linux, Mac or PC computers.

In the end, cloud gaming should focus on providing the best possible user experience, and any progress with the development; especially on reducing latency, controller support, hardware input etc. will be posted here on the client's open source page. Have a suggestion or an idea for the project? Go right ahead and contribute - everyone is welcome!

## What is cloud gaming?
The idea of playing pc games over the air, by letting another (more powerful) gaming machine run the applications for you, originally came into light in the early 2007, and many have since then speculated when the technology would allow for [cloud gaming to actually work well](https://www.snoost.com/en/cloud-gaming-guide/), especially since there has been so many challenges to the idea of streaming a video game over the air, such as latency, distance, network connection, streaming software etc.

Cloud gaming as a term generally also covers the concept of letting publishers and developers utilise cloud processing, to handle online game servers and tasks, which are normally quite resource intensive - this especially goes for the modern era of video games, which implements more and more deep learning techniques to server its user base. In these cases, the cloud servers process the heavy tasks, and stream the data and results to the users' clients, which then display or further process the data and use that information in-game. However, with projects like Snoost the term "cloud gaming" most frequently means "to let an external gaming rig run and stream the PC game, and let your local PC control it remotely", typically also referred to as an interactive stream.

### Client to server relationship
Before we proceed with digging deeper into cloud gaming, it is important to first understand the client/server relationship, as these are the two essential parties that make up the need and concept of streaming games over the air. The need for cloud gaming is made up of the constantly increasing hardware demands for computer games, and thus the client (the user's own local computer) would otherwise need to be kept upgraded, or replaced, from time to time to keep up with the ever increasing graphic and processing requirements of modern video games.

In general, the latency is heavily influenced by the distance between the client and the server, so it is vital to connect to the closest server possible.

#### Streaming client
When mentioning *the client* we refer to the specific user's own computer, from which he or she would like to play a video game. The client does not need to meet the game's demands, which means that even low-end computers, netbooks and otherwise less powerful laptops (such as a MacBook Air), should be able to play the desired game despite not meeting the requirements to do so. In short, **the client does not run the game**, but only *receives a video stream* from the cloud gaming server. It is the server which actually runs the game, and therefore the server's resources which are being used to render everything for the game to be playable.

The client is still perfectly able to utilise external monitors, attached keyboards and mice, and should even be able to preserve battery life and its general lifespan better, since the game streaming requires far less resources than if it had otherwise run the game locally.

The main tasks of the client is to be able to send input signals to the server, and receive and render a stream from the server with minimal latency.

This project mainly focuses on the client, and the repository does therefore solely contain code for the client part of cloud gaming.

#### Streaming server
In contrast to the client, the server is the machine that actually runs the game and renders the 3D (if relevant). The server's main responsibilities are; running the actual game, receiving the input signals from the client and returning a video stream of the game (in the appropriate resolution) so that the client will be able to see what is going on.

### Streaming games to your PC
Basically, cloud gaming refers to the ability to stream PC games (from a cloud gaming server) to almost any computer, regardless of which operating system that computer is running, and without having to meet the game's required specifications, such as graphic cards, RAM, processor power etc., since all of these requirements will already have been met by the cloud gaming server, which then runs the game from a distance and streams the game directly to the client.

#### Latency issues
Obviously, latency plays a big part when streaming anything over the internet. However, for most streaming services the end-user does not necessarily feel affected by latency, since streaming - in its traditional sense - is a one-way communication, such as just "delivering" a stream (of either video, audio or both) from a host server to a client PC. In the case of cloud gaming, it's a bit different though, since the stream is interactive and thus requires input from the user as well, meaning the user will have to first send some input (in terms of the mouse or keyword), and then wait for the cloud gaming server to process the input, reflect it in the game, and return the response (in terms of the video of the PC game being played) directly back to the client.

This whole process of course adds to the feeling of there being a slight delay (or latency/ping if you will), which highly depends on one's proximity between your own PC and the cloud gaming server the game is being played from.

#### Input and interactive stream
As opposed to traditional streaming services, which only stream video and/or audio, game streaming requires a much more interactive setup, since the user should be able to control what is actually going on in the stream (or the video game, if you will). The fact that the server expects an input from the client, and then has to process that input before returning it, means that latency plays are more important role, since the user would experience a certain delay (the time it takes from the input is sent, until the server has processed it and sent it back) while playing the game. If the server and client are relatively close to each other, the distance wouldn't produce any significant delay, and only the processing as well as encoding and decoding of the signal would play a role in the amount of latency, ping or delay, that the user experiences during the gameplay.

#### Client computer requirements
Basically, the client computer will only require some basic hardware specifications, such as hardware decoding, to be able to receiving and render the game stream from the cloud gaming server. In essence, as long as the computer is able to stream movies from Netflix or other video streaming services, it should also be compatible from this cloud gaming software. That said however, there are some other factors playing in to a game streaming setup, that are not necessarily relevant for traditional streaming services. Decoding of the signal for instances, plays a larger role with cloud gaming, since decoding the streaming is a local task, and if the client computer is too slow, it might produce a noticeable extra delay between sending the input from keyboard and mouse, and actually seeing the result in the video stream.

Usually though, most low-end computers bought within the last 5-6 years, should handle this just fine, and will decode the signal quite quickly.

Another factor is the WIFI or internet connection, since any interference from other devices on the same network may worsen the experience, since cloud gaming heavily relies on being able to download at a high bitrate for a smooth experience. A download speed of a least 25 Mbps is recommended, although slower may be able to produce good results as well.

### Unlimited hours of gaming
Combined with the Snoost cloud gaming service, this client gives users an unlimited amount of gaming hours.

Unlike [other practices in cloud gaming services](https://en.wikipedia.org/wiki/Cloud_gaming), Snoost provides a completely unrestricted access to the cloud gaming servers, in the sense that users may play for as many hours as they like, without having to consider the amount of time they play, or worry about hitting the monthly limit governed by which plan they are on.

In general, playing video games of any kind should be an experience where gamers can sit down and fully emerge themselves in the game they are playing, and experience the game without distractions and without having to skip long cut scenes, just because it cuts into the time they are allowed to play each month. With Snoost Cloud Gaming, and the servers this project's game streaming client connects to, gamers will never fall trap to any monthly limits, since Snoost provides unlimited hours of gaming without any increased costs.

### Play any game on any PC, even Linux and Macs
Cloud gaming allows you to play any game that the remote PC is capable of playing, but without requiring much of the locale device. This means that it is possible to play some of the newer and more modern PC games, even if your own computer is incompatible, by simply letting a remote server running Windows play the games for you, and thereby being able to stream it directly to your own MacBook, Linux or PC regardless of which operating system you are using, and thus making it seem like the game is actually being played on your own computer (which it - in reality - is not).

It is always been somewhat of an issue with OS compatibility when speaking of games (or any software for that matter), since there are many differentiating factors (such as filesystems, architecture etc.) between the operating systems, which all make it a challenge for game developers to enable playing their games on each platform. It also typically comes down to which game engine the publisher chooses to base its game on, and even some of the previously more popular engines (such as the Crytek engine) have only been working on Windows, making it almost impossible for developers to release the games on other platforms like Mac and Linux.

So although we have more recently, both for Indie developers and established publishers, been seeing more trends towards shifting to multi compatible game engines, it is still an issue that sometimes forces developers to compromise on the game's development, either by requiring developers to spend a lot of time just making the game compatible, or by pushing them to completely switch engines - for instance like Star Citizen did from Frostbite to Amazon Lumberyard.

With cloud gaming however, all of this is simply not an issue, since the cloud gaming servers would be running Windows, the by far most used platform for game releases, which makes almost any computer able to play games regardless of the operating system. Even huge graphically intensive games like [Star Citizen, runs perfectly and without issue on MacBooks and Linux machines](https://www.snoost.com/en/blog/updated-guide-to-play-star-citizen-on-any-computer/), making the game available to a much larger audience than had it only been available on Windows (or Linux, now that they are developing the game on Amazon's Lumberyard). And of course users also avoid the hardcore system requirements for some of the more modern games, which they would otherwise not be able to play even on a Windows PC, since their computers simply are not powerful enough. All solved, by letting a remote gaming rig take care of the processing, rendering and running of the game itself, instead of running it locally on the client computer.

## Final words on gaming remotely
We truly hope that projects like this can help cloud gaming technologies advance, so that playing games remotely through a cloud server becomes a more common way to play video games, especially on MacBooks and Linux machines, which are typically not as compatible with PC games as Windows machines usually are.

### How to contribute to the streaming client
All of the code for the game streaming client is available here on bitbucket, and everyone is free to suggest changes or contribute in any other way, as the ultimate goal of the project is simply to enhance the performance and gaming experience for the users who use it.

Remember that everyone is welcome, and all ideas for contributions will be considered for implementation, as long as they assist in either directly or indirectly advance the idea of streaming games from a cloud server with minimal latency.